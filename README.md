# chuck-norris-graphql

[GraphQl](http://facebook.github.io/graphql/October2016/) API that exposes jokes about Chuck Norris. It includes [GraphiQl](https://github.com/graphql/graphiql) for exploring the API.

## Getting started

The application is build using [TypeScript](https://www.typescriptlang.org/). Therefor you'll need [Node.js and npm](https://nodejs.org/en/) first, before proceeding with any of the steps below.

Running the application can be done by following the steps below.

1. Install NPM modules: `npm install`
2. Build the application: `npm run build`
3. Start the application: `npm start`

The `/graphql` endpoint is now up and running to receive POST requests. The in-browser IDE GraphiQl is reachable at `/graphiql` also.

### Developing

While developing you can use the commands below to automatically build and refresh upon a code change:

1. Install NPM modules: `npm install`
2. Start build in watch mode: `npm run watch`
3. Serve and reload upon changes: `npm run serve`

### NPM-run-scripts

Start the scripts below using: `npm run <command>`

* `start`: start the application after you've done a build.
* `test`: test your source code (linting only for now).
* `build`: build source code.
* `watch`: auto-compile TypeScript when making changes.
* `serve`: serve your compiled files during development and reload upon change.
* `lint`: lint your files
* `tsc`: run the TyeScript compiler from the local repository (and not globally installed). Add commands after a `--` like `npm run tsc -- --help`

- - - -

## User stories

### #1: Continuous testing (10 points)

As a developer, I want to ensure our code adheres to our standards and can be built. Create a pipeline to automatically run linting and build upon commit.

**Requirements:**

* Create a pipeline to lint and build changes upon a commit to the remote repository.

### #2: Repository configuration (5 points)

As a developer, I want to ensure people work in their own branches, to keep our repository organized. Set up branch permissions in the repository to force people to develop in their own branches.

**Requirements:**

* Ensure no one can push directly to testing, staging, or production.
* Ensure merges can only happen if there was at least one successful build.

### #3: Continuous deployment to testing (10 points)

As a developer, I want to test new features quickly. Create a pipeline to automatically deploy new feature to our testing environment.

**Requirements:**

* Create a pipeline to automatically deploy new versions of the API to our testing environment.
* Before deploying, ensure the code is linted and built a final time, to ensure the merged code is still proper.
* Do not commit any usernames or passwords to the repository.

### #4: Continuous deployment of staging & production (5 points)

As a customer, I want new Chuck Norris jokes to be deployed to our production once they have been tested. Create a pipeline to automatically deploy the staging and production branches to their respective environments.

**Requirements:**

* Create a pipeline to automatically deploy new changes on the staging and production branches to their respective environments.
* Before deploying, ensure the code is linted and built a final time, to ensure the merged code is still proper.
* Do not commit any usernames or passwords to the repository.

### #5: Automatic PR creation (15 points)

As a developer, I want to ensure no change to the API is forgotten during development. Automatically create a PR from bugfix or feature branches to staging, once they have been merged into testing.

**Requirements:**

* Create a pipeline steps to automatically create a PR from bugfix or feature branches, once they have been merged into testing.
* Do not commit any usernames or passwords to the repository.

### #6: Service account for PR creation (5 points)

As a developer, I want to ensure my pipeline isn't dependent on a developer's personal credentials. Set up the pipeline for PR creation to use an app password, not a developer's credentials.

**Requirements:**

* Use an app password for the pipeline that automatically creates PRs to staging.
* Do not commit any usernames or passwords to the repository.

### #7: Automatic versioning (10 points)

As a tester, I want to know which version of the API is deployed. Set up a pipeline for automatic versioning for the API.

**Requirements:**

* Create a pipeline to automatically bump the version of the API when a change is merged into staging.
  * Bump the patch version number on a merge from a bugfix branch.
  * Bump the minor version number on a merge from a feature branch.
* Do not commit any usernames or passwords to the repository.

### #8: Efficient automatic deployment (5 points)

As a developer, I want to ensure my deployments are as efficient as possible. Modify the deployment pipeline to only deploy the built code, not everything contained in the repository.

**Requirements:**

* Modify the deployment pipeline to deploy only the content of the build (`/dist`) folder and the package.json file.

### #9: API improvements (5 points)

As a customer, I want to keep expanding my API with new jokes. Add a Chuck Norris joke to the API.

**Requirements:**

* Find a worthy Chuck Norris joke and add it to the API.
* Add the proper author of the joke.
